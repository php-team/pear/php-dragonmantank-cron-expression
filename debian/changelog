php-dragonmantank-cron-expression (3.4.0-1) unstable; urgency=medium

  * New upstream version 3.4.0
  * Use my debian.org email in Uploaders

 -- Robin Gustafsson <rgson@debian.org>  Mon, 03 Mar 2025 20:49:00 +0100

php-dragonmantank-cron-expression (3.3.2-4) unstable; urgency=medium

  * Team upload
  * Revert "Force system dependencies loading"
  * Modernize PHPUnit syntax
  * Update Standards-Version to 4.7.2

 -- David Prévot <taffit@debian.org>  Fri, 28 Feb 2025 07:03:18 +0100

php-dragonmantank-cron-expression (3.3.2-3) unstable; urgency=medium

  * Team upload
  * Drop incompatible -v option for PHPUnit 11
  * Make provider functions static (PHPUnit 11 Fix) (Closes: #1070538)

 -- David Prévot <taffit@debian.org>  Fri, 11 Oct 2024 15:51:00 +0100

php-dragonmantank-cron-expression (3.3.2-2) unstable; urgency=medium

  * Team upload
  * Force system dependencies loading

 -- David Prévot <taffit@debian.org>  Fri, 08 Mar 2024 10:43:59 +0100

php-dragonmantank-cron-expression (3.3.2-1) unstable; urgency=medium

  * New upstream version 3.3.2
  * Update standards version to 4.6.2, no changes needed.
  * Remove obsolete patch

 -- Robin Gustafsson <robin@rgson.se>  Wed, 28 Jun 2023 22:58:08 +0200

php-dragonmantank-cron-expression (3.3.1-2) unstable; urgency=medium

  [ Athos Ribeiro ]
  * Skip unstable PHP 8.1 tests. (Closes: #1015027)

 -- Robin Gustafsson <robin@rgson.se>  Fri, 02 Sep 2022 23:29:26 +0200

php-dragonmantank-cron-expression (3.3.1-1) unstable; urgency=medium

  * New upstream version 3.3.1

 -- Robin Gustafsson <robin@rgson.se>  Thu, 14 Apr 2022 11:27:04 +0200

php-dragonmantank-cron-expression (3.1.0-1) experimental; urgency=medium

  * Team upload of new major upstream version to experimental

  [ Robin Gustafsson ]
  * Remove Salsa CI config
  * Bump Standards-Version
  * Replace git attributes with uscan's gitexport=all
  * Rename main branch to debian/latest (DEP-14)
  * Mark test dependencies with build profile spec

  [ David Prévot ]
  * New upstream version 3.1.0
  * Update standards version to 4.6.0, no changes needed.
  * Adapt package to new php-webmozart-assert dependency
  * Install dh-sequence-* instead of using dh --with
  * Install /u/s/p/autoloaders file
  * Declare package as Multi-Arch: foreign

 -- David Prévot <taffit@debian.org>  Mon, 03 Jan 2022 17:22:53 -0400

php-dragonmantank-cron-expression (2.3.1-1) unstable; urgency=medium

  * New upstream version 2.3.1

 -- Robin Gustafsson <robin@rgson.se>  Tue, 24 Nov 2020 20:27:33 +0100

php-dragonmantank-cron-expression (2.3.0-1) unstable; urgency=medium

  * Initial release (Closes: #951162)

 -- Robin Gustafsson <robin@rgson.se>  Fri, 08 May 2020 00:03:11 +0200
